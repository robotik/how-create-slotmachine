# SlotMachine

Using an HTML5 canvas and RWD, a slot machine's Vue component was created. [Read More Here](https://pokieslab.com/online-casinos/) to see all the slots from this page, there are many interesting slots, with a first deposit bonus. If you as well as I like to play online slots, then my roulette for you. My personal recommendation is buffalo slot. For the development of this site were connected 2 programmers, backend and frontend, all the source material you can find below. And if you still decide to play on the site, have a nice game)

<h2 dir="auto">Installation</h2>
<h3 dir="auto"><a id="user-content-in-npm" class="anchor" href="https://github.com/puckwang/vue-slot-machine#in-npm" aria-hidden="true"></a>In NPM</h3>
<ol dir="auto">
<li>Use&nbsp;<code>npm</code>&nbsp;or&nbsp;<code>yarn</code>&nbsp;download&nbsp;<code>@puckwang/vue-slot-machine</code>.</li>
</ol>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto">
<pre>npm install @puckwang/vue-slot-machine

// or

yarn add @puckwang/vue-slot-machine</pre>
</div>
<ol dir="auto" start="2">
<li>Registered component. Then register the plugin to globally install all components:</li>
</ol>
<div class="highlight highlight-source-js notranslate position-relative overflow-auto" dir="auto">
<pre><span class="pl-k">import</span> <span class="pl-v">SlotMachine</span> <span class="pl-k">from</span> <span class="pl-s">'@puckwang/vue-slot-machine'</span><span class="pl-kos">;</span>

<span class="pl-v">Vue</span><span class="pl-kos">.</span><span class="pl-en">use</span><span class="pl-kos">(</span><span class="pl-v">SlotMachine</span><span class="pl-kos">)</span><span class="pl-kos">;</span></pre>
</div>
<p dir="auto">Or, import components individually for local registration:</p>
<div class="highlight highlight-source-js notranslate position-relative overflow-auto" dir="auto">
<pre><span class="pl-k">import</span> <span class="pl-kos">{</span><span class="pl-v">SlotMachine</span><span class="pl-kos">}</span> <span class="pl-k">from</span> <span class="pl-s">'@puckwang/vue-slot-machine'</span><span class="pl-kos">;</span>

<span class="pl-k">export</span> <span class="pl-k">default</span> <span class="pl-kos">{</span>
    <span class="pl-c1">components</span>: <span class="pl-kos">{</span> <span class="pl-s">"slot-machine"</span>: <span class="pl-v">SlotMachine</span> <span class="pl-kos">}</span>
<span class="pl-kos">}</span></pre>
</div>
<h3 dir="auto"><a id="user-content-in-browser" class="anchor" href="https://github.com/puckwang/vue-slot-machine#in-browser" aria-hidden="true"></a>In Browser</h3>
<div class="highlight highlight-text-html-basic notranslate position-relative overflow-auto" dir="auto">
<pre><span class="pl-kos">&lt;</span><span class="pl-ent">div</span> <span class="pl-c1">id</span>="<span class="pl-s">app</span>"<span class="pl-kos">&gt;</span>
    <span class="pl-kos">&lt;</span><span class="pl-ent">div</span><span class="pl-kos">&gt;</span>
        <span class="pl-kos">&lt;</span><span class="pl-ent">slot-machine</span><span class="pl-kos">&gt;</span><span class="pl-kos">&lt;/</span><span class="pl-ent">slot-machine</span><span class="pl-kos">&gt;</span>
    <span class="pl-kos">&lt;/</span><span class="pl-ent">div</span><span class="pl-kos">&gt;</span>
<span class="pl-kos">&lt;/</span><span class="pl-ent">div</span><span class="pl-kos">&gt;</span>

<span class="pl-kos">&lt;</span><span class="pl-ent">script</span> <span class="pl-c1">src</span>="<span class="pl-s">https://unpkg.com/@puckwang/vue-slot-machine@latest</span>"<span class="pl-kos">&gt;</span><span class="pl-kos">&lt;/</span><span class="pl-ent">script</span><span class="pl-kos">&gt;</span>

<span class="pl-kos">&lt;</span><span class="pl-ent">script</span><span class="pl-kos">&gt;</span>
<span class="pl-k">var</span> <span class="pl-s1">app</span> <span class="pl-c1">=</span> <span class="pl-k">new</span> <span class="pl-v">Vue</span><span class="pl-kos">(</span><span class="pl-kos">{</span>
  <span class="pl-c1">el</span>: <span class="pl-s">'#app'</span><span class="pl-kos">,</span>
  <span class="pl-c1">data</span>: <span class="pl-kos">{</span>
    <span class="pl-c1">message</span>: <span class="pl-s">'Hello Vue!'</span>
  <span class="pl-kos">}</span><span class="pl-kos">,</span>
  <span class="pl-c1">components</span>: <span class="pl-kos">{</span> <span class="pl-s">"slot-machine"</span>: <span class="pl-v">VueSlotMachine</span><span class="pl-kos">.</span><span class="pl-c1">SlotMachine</span> <span class="pl-kos">}</span>
<span class="pl-kos">}</span><span class="pl-kos">)</span>
<span class="pl-kos">&lt;/</span><span class="pl-ent">script</span><span class="pl-kos">&gt;</span></pre>
</div>
<h2 dir="auto"><a id="user-content-usage" class="anchor" href="https://github.com/puckwang/vue-slot-machine#usage" aria-hidden="true"></a>Usage</h2>
<p dir="auto"><a href="https://codepen.io/puckwang/pen/OGvrdM" rel="nofollow">Demo</a></p>
<div class="highlight highlight-text-html-basic notranslate position-relative overflow-auto" dir="auto">
<pre><span class="pl-kos">&lt;</span><span class="pl-ent">template</span><span class="pl-kos">&gt;</span>
    <span class="pl-kos">&lt;</span><span class="pl-ent">slot-machine</span>
        <span class="pl-c1">:list</span>="<span class="pl-s">list</span>"
        <span class="pl-c1">:trigger</span>="<span class="pl-s">trigger</span>"
        <span class="pl-c1">:height</span>="<span class="pl-s">300</span>"
        <span class="pl-c1">:width</span>="<span class="pl-s">300</span>"
        <span class="pl-c1">@onComplete</span>="<span class="pl-s">onComplete</span>"<span class="pl-kos">&gt;</span>
    <span class="pl-kos">&lt;/</span><span class="pl-ent">slot-machine</span><span class="pl-kos">&gt;</span>
<span class="pl-kos">&lt;/</span><span class="pl-ent">template</span><span class="pl-kos">&gt;</span>

<span class="pl-kos">&lt;</span><span class="pl-ent">script</span><span class="pl-kos">&gt;</span>
<span class="pl-k">export</span> <span class="pl-k">default</span> <span class="pl-kos">{</span>
    <span class="pl-en">data</span><span class="pl-kos">(</span><span class="pl-kos">)</span> <span class="pl-kos">{</span>
        <span class="pl-k">return</span> <span class="pl-kos">{</span>
            <span class="pl-c1">list</span>: <span class="pl-kos">[</span>
                <span class="pl-kos">{</span><span class="pl-c1">text</span>: <span class="pl-s">'1'</span><span class="pl-kos">,</span> <span class="pl-c1">color</span>: <span class="pl-s">'#668CFF'</span><span class="pl-kos">}</span><span class="pl-kos">,</span>
                <span class="pl-kos">{</span><span class="pl-c1">text</span>: <span class="pl-s">'2'</span><span class="pl-kos">,</span> <span class="pl-c1">color</span>: <span class="pl-s">'#FF6666'</span><span class="pl-kos">}</span><span class="pl-kos">,</span>
                <span class="pl-kos">{</span><span class="pl-c1">text</span>: <span class="pl-s">'3'</span><span class="pl-kos">,</span> <span class="pl-c1">color</span>: <span class="pl-s">'#B366FF'</span><span class="pl-kos">}</span><span class="pl-kos">,</span>
            <span class="pl-kos">]</span><span class="pl-kos">,</span>
            <span class="pl-c1">trigger</span>: <span class="pl-c1">null</span><span class="pl-kos">,</span>
        <span class="pl-kos">}</span><span class="pl-kos">;</span>
    <span class="pl-kos">}</span><span class="pl-kos">,</span>
    <span class="pl-c1">methods</span>: <span class="pl-kos">{</span>
        <span class="pl-en">start</span><span class="pl-kos">(</span><span class="pl-kos">)</span> <span class="pl-kos">{</span> <span class="pl-c">// Trigger to let the machine start</span>
            <span class="pl-smi">this</span><span class="pl-kos">.</span><span class="pl-c1">trigger</span> <span class="pl-c1">=</span> <span class="pl-k">new</span> <span class="pl-v">Date</span><span class="pl-kos">(</span><span class="pl-kos">)</span><span class="pl-kos">;</span>
        <span class="pl-kos">}</span><span class="pl-kos">,</span>
        <span class="pl-en">onComplete</span><span class="pl-kos">(</span><span class="pl-s1">data</span><span class="pl-kos">)</span> <span class="pl-kos">{</span> <span class="pl-c">// Run complete callback</span>
            <span class="pl-smi">console</span><span class="pl-kos">.</span><span class="pl-en">log</span><span class="pl-kos">(</span><span class="pl-s1">data</span><span class="pl-kos">)</span><span class="pl-kos">;</span>
        <span class="pl-kos">}</span>
    <span class="pl-kos">}</span>
<span class="pl-kos">}</span>
<span class="pl-kos">&lt;/</span><span class="pl-ent">script</span><span class="pl-kos">&gt;</span></pre>
</div>
<h2 dir="auto"><a id="user-content-configuration" class="anchor" href="https://github.com/puckwang/vue-slot-machine#configuration" aria-hidden="true"></a>Configuration</h2>
<h3 dir="auto"><a id="user-content-component-props" class="anchor" href="https://github.com/puckwang/vue-slot-machine#component-props" aria-hidden="true"></a>Component props</h3>
<h5 dir="auto"><a id="user-content-list" class="anchor" href="https://github.com/puckwang/vue-slot-machine#list" aria-hidden="true"></a><code>list</code></h5>
<ul dir="auto">
<li>Type:&nbsp;<a href="https://github.com/puckwang/vue-slot-machine#gift-object">Object</a>[]</li>
<li>Description: A list of gift object.</li>
<li>Default: 0~9</li>
</ul>
<h5 dir="auto"><a id="user-content-trigger" class="anchor" href="https://github.com/puckwang/vue-slot-machine#trigger" aria-hidden="true"></a><code>trigger</code></h5>
<ul dir="auto">
<li>Type: Date</li>
<li>Description: Use to trigger to let the machine start.</li>
<li>Default: null</li>
</ul>
<h5 dir="auto"><a id="user-content-currentindex" class="anchor" href="https://github.com/puckwang/vue-slot-machine#currentindex" aria-hidden="true"></a><code>currentIndex</code></h5>
<ul dir="auto">
<li>Type: Integer</li>
<li>Description: Specify the index the result.</li>
<li>Default: -1</li>
</ul>
<h5 dir="auto"><a id="user-content-width" class="anchor" href="https://github.com/puckwang/vue-slot-machine#width" aria-hidden="true"></a><code>width</code></h5>
<ul dir="auto">
<li>Type: Date</li>
<li>Description: Width of canvas.</li>
<li>Default: 300</li>
</ul>
<h5 dir="auto"><a id="user-content-height" class="anchor" href="https://github.com/puckwang/vue-slot-machine#height" aria-hidden="true"></a><code>height</code></h5>
<ul dir="auto">
<li>Type: Date</li>
<li>Description: Height of canvas.</li>
<li>Default: 300</li>
</ul>
<h5 dir="auto"><a id="user-content-responsive" class="anchor" href="https://github.com/puckwang/vue-slot-machine#responsive" aria-hidden="true"></a><code>responsive</code></h5>
<ul dir="auto">
<li>Type: Boolean</li>
<li>Description: Responsive Canvas, width and height will be filled parent element, it's according to the set ratio (height / width).</li>
<li>Default: false</li>
</ul>
<h3 dir="auto"><a id="user-content-gift-object" class="anchor" href="https://github.com/puckwang/vue-slot-machine#gift-object" aria-hidden="true"></a>Gift Object</h3>
<h5 dir="auto"><a id="user-content-text" class="anchor" href="https://github.com/puckwang/vue-slot-machine#text" aria-hidden="true"></a><code>text</code></h5>
<ul dir="auto">
<li>Type: String</li>
<li>Description: Text of the gift.</li>
<li>Required</li>
</ul>
<h5 dir="auto"><a id="user-content-color" class="anchor" href="https://github.com/puckwang/vue-slot-machine#color" aria-hidden="true"></a><code>color</code></h5>
<ul dir="auto">
<li>Type: ColorHex</li>
<li>Description: Color for gift text.</li>
<li>Default: #000 (Black)</li>
</ul>
<h5 dir="auto"><a id="user-content-fontfamily" class="anchor" href="https://github.com/puckwang/vue-slot-machine#fontfamily" aria-hidden="true"></a><code>fontFamily</code></h5>
<ul dir="auto">
<li>Type: String</li>
<li>Description: Font family for gift text.</li>
<li>Default: Arial</li>
</ul>
<h5 dir="auto"><a id="user-content-fontstyle" class="anchor" href="https://github.com/puckwang/vue-slot-machine#fontstyle" aria-hidden="true"></a><code>fontStyle</code></h5>
<ul dir="auto">
<li>Type: String</li>
<li>Description: Font style for gift text. can be&nbsp;<code>normal</code>,&nbsp;<code>bold</code>,&nbsp;<code>italic</code>&nbsp;or even&nbsp;<code>italic bold</code>.</li>
<li>Default: normal</li>
</ul>
<h5 dir="auto"><a id="user-content-align" class="anchor" href="https://github.com/puckwang/vue-slot-machine#align" aria-hidden="true"></a><code>align</code></h5>
<ul dir="auto">
<li>Type: String</li>
<li>Description: Text align for gift text. can be&nbsp;<code>left</code>,&nbsp;<code>center</code>, or&nbsp;<code>right</code>.</li>
<li>Default:&nbsp;<code>center</code></li>
</ul>
<h5 dir="auto"><a id="user-content-verticalalign" class="anchor" href="https://github.com/puckwang/vue-slot-machine#verticalalign" aria-hidden="true"></a><code>verticalAlign</code></h5>
<ul dir="auto">
<li>Type: String</li>
<li>Description: Text vertical align for gift text. can be&nbsp;<code>top</code>,&nbsp;<code>middle</code>&nbsp;or&nbsp;<code>bottom</code>.</li>
<li>Default:&nbsp;<code>middle</code></li>
</ul>
<blockquote>
<p dir="auto">Changing the&nbsp;<code>fontFamily</code>,&nbsp;<code>fontStyle</code>&nbsp;and&nbsp;<code>verticalAlign</code>&nbsp;will cause the layout to break! It is recommended to use the default value.</p>
</blockquote>
